const { priorityDataSource } = require('./utils');

class DataGeneratorService {

  #rowsToDo = 1000 - 3;

  constructor() { }

  #generateRandomNumber = (from = 0, to = 100) => {
    return (Math.trunc(Math.random() * 1000000) % (to + 1)) + from;
  };

  #getDiffBetweenDatesInDays = (d1, d2) => {
    const diffTime = Math.abs(d2.valueOf() - d1.valueOf());
    const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
    return diffDays;
  }

  #generateId(forbiddenIds) {
    const newId = this.#generateRandomNumber(0, 1000);
    if (forbiddenIds.includes(newId)) return this.#generateId(forbiddenIds);
    forbiddenIds.push(newId);
    return newId;
  }

  #generateSubtasks(isLast, forbiddenIds) {
    const amount = isLast ? this.#rowsToDo : this.#generateRandomNumber(0, 500);
    this.#rowsToDo = this.#rowsToDo - amount;
    return Array.from({ length: amount }).map((_, index) => this.#generateRandomData(this.#generateId(forbiddenIds), `Subtask ${index + 1}`));
  }

  #generateRandomData(id, name) {
    let startDate = new Date();
    startDate.setDate(28 - this.#generateRandomNumber(0, 27));
    startDate.setMonth(11 - this.#generateRandomNumber(0, 11));

    let endDate = new Date();
    endDate.setDate(28 - this.#generateRandomNumber(0, 27));
    endDate.setMonth(11 - this.#generateRandomNumber(0, 11));

    if (startDate > endDate) {
      [startDate, endDate] = [endDate, startDate];
    }
    return {
      taskID: id,
      taskName: name,
      startDate: startDate,
      endDate: endDate,
      duration: this.#getDiffBetweenDatesInDays(startDate, endDate),
      priority: priorityDataSource[this.#generateRandomNumber(0, 2)]
    }
  }

  generateData() {
    const forbiddenIds = [1,2,3];
      return [
        { id: 2, name: 'Design' },
        { id: 3, name: 'Implementation Phase' },
        { id: 1, name: 'Planning' }]
      .map(({ id, name }, index) => {
        const data = this.#generateRandomData(id, name);
        data.subtasks = this.#generateSubtasks(index === 2, forbiddenIds);
        return data;
      });
  }
}

const dataGenerator = new DataGeneratorService();

module.exports = {
  dataGenerator
}
