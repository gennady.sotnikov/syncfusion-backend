const { dataService } = require('./data.service');
const { defaultColumns } = require('./utils');

class ColumnsService {

  #columns = [...defaultColumns];

  get columns() {
    return this.#columns;
  }

  setColumns = (columns) => {
    this.#columns = columns;
  }

  editColumn = (name, data) => {
    const columnsCopy = [...this.#columns];
    let columnInd = columnsCopy.findIndex(c => name === c.name);
    if (columnInd !== -1) {
      if (columnsCopy[columnInd].dataType !== data.dataType) {
        dataService.editColumnValue(name, data.defaultValue);
      }
      if (name !== data.name) {
        dataService.renameColumn(name, data.name);
      }
      columnsCopy[columnInd] = data;
      this.#columns = columnsCopy;
      return this.#columns;
    } else {
      throw new Error('Column isn\'t found!');
    }
  }
}

const columnsService = new ColumnsService();

module.exports = {
  columnsService
}
