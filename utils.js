const TextAlign = {
  Right: 'Right', Left: 'Left', Center: 'Center'
}

const Priority = {
  High: 'High',
  Medium: 'Normal',
  Low: 'Low'
}

const defaultText = 'Default Text';

const DataTypes = {
  Text: 'string', Num: 'numeric', Date: 'datepicker', Boolean: 'boolean', DropDownList: 'dropdown'
}

const TextWrap = {
  Normal: 'normal',
  BreakWord: 'break-word'
}

const defaultFontSize = 12;

const defaultBackgroundColor = 'rgba(0,0,0,0)';

const defaultStyles = Object.freeze({
  textAlign: TextAlign.Left,
  fontSize: defaultFontSize,
  fontColor: 'black',
  backgroundColor: defaultBackgroundColor,
  textWrap: TextWrap.Normal,
});

const Fields = {
  TaskID: 'taskID',
  TaskName: 'taskName',
  StartDate: 'startDate',
  EndDate: 'endDate',
  Duration: 'duration',
  Priority: 'priority'
}

const priorityDataSource = Object.freeze([Priority.High, Priority.Medium, Priority.Low]);

const defaultColumns = Object.freeze([{
  ...defaultStyles,
  name: Fields.TaskID,
  isPrimary: true,
  width: 120,
  headerText: 'Task ID',
  dataType: DataTypes.Num,
  defaultValue: 1,
}, {
  ...defaultStyles,
  name: Fields.TaskName,
  isPrimary: false,
  minWidth: 190, // it looks like minWidth doesn't work
  width: 210,
  headerText: 'Task Name',
  dataType: DataTypes.Text,
  defaultValue: defaultText,
}, {
  ...defaultStyles,
  name: Fields.StartDate,
  isPrimary: false,
  width: 170,
  headerText: 'Start Date',
  dataType: DataTypes.Date,
  defaultValue: new Date()
}, {
  ...defaultStyles,
  name: Fields.EndDate,
  isPrimary: false,
  width: 170,
  headerText: 'End Date',
  dataType: DataTypes.Date,
  defaultValue: new Date()
}, {
  ...defaultStyles,
  name: Fields.Duration,
  isPrimary: false,
  width: 140,
  dataType: DataTypes.Num,
  headerText: 'Duration',
  defaultValue: 0
}, {
  ...defaultStyles,
  name: Fields.Priority,
  isPrimary: false,
  width: 120,
  dataType: DataTypes.DropDownList,
  dataSource: [...priorityDataSource],
  headerText: 'Priority',
  defaultValue: Priority.Low
}]);

const indexIsValid = (index) => {
  return index && Array.isArray(index) && index.length;
}

module.exports = {
  defaultColumns,
  priorityDataSource,
  indexIsValid
}
