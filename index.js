const express = require('express');
const cors = require('cors');
const http = require('http');

const { columnsService } = require('./columns.service');
const { dataService } = require('./data.service');
const { indexIsValid } = require('./utils');

const app = express();

const server = http.createServer(app);

const columnsEndpoint = '/columns';

const dataEndpoint = '/data';
const success = Object.freeze({ 'success': true });

const { Server } = require("socket.io");
const io = new Server(server);

server.listen(3001, () => {
  console.log('listening on *:3001');
});

const needUpdate = 'need update';

app.use(express.json());

app.use(cors());

app.get(columnsEndpoint, (req, res) => {
  return res.json(columnsService.columns);
});

app.post(columnsEndpoint, (req, res) => {
  const { data, socketId } = req.body;
  columnsService.setColumns(data);
  io.sockets.except(socketId).emit(needUpdate);
  return res.json(success);
});

app.post(columnsEndpoint + '/edit', (req, res) => {
  const { name, data, socketId } = req.body;
  if (!name || !data) {
    return res.status(404);
  }
  io.sockets.except(socketId).emit(needUpdate)
  return res.json(columnsService.editColumn(name, data));
});

app.post(dataEndpoint + '/edit', (req, res) => {
  const { index, data, socketId } = req.body;
  if (!(indexIsValid(index) && data)) {
    return res.status(404);
  }
  dataService.editRow(index, data);
  io.sockets.except(socketId).emit(needUpdate)
  return res.json(success);
});

app.get(dataEndpoint, (req, res) => {
  return res.json(dataService.data);
});

app.post(dataEndpoint + '/delete', (req, res) => {
  const { indexes, socketId } = req.body;
  if (!(indexIsValid(indexes) && Array.isArray(indexes[0]))) {
    return res.status(404).end('Validation Error');
  }
  dataService.deleteRows(indexes);
  io.sockets.except(socketId).emit(needUpdate)
  return res.json(success);
});

app.post(dataEndpoint + '/asChild', (req, res) => {
  const { target, value, socketId } = req.body;
  if (!(indexIsValid(target))) {
    return res.status(404).end('Validation Error');
  }
  dataService.addAsChild(target, value);
  io.sockets.except(socketId).emit(needUpdate)
  return res.json(success);
});

app.post(dataEndpoint + '/asSubling', (req, res) => {
  const { targetIndex, data, isNext, socketId } = req.body;
  if (!(indexIsValid(targetIndex) && data && isNext !== undefined)) {
    return res.status(404).end('Validation Error');
  }
  dataService.addAsSubling(targetIndex, data, isNext);
  io.sockets.except(socketId).emit(needUpdate)
  return res.json(success);
});

app.post(dataEndpoint + '/move', (req, res) => {
  const { fromArr, toArr, socketId } = req.body;
  if (!(indexIsValid(fromArr) && indexIsValid(toArr) && indexIsValid(fromArr[0]) && indexIsValid(toArr[0]))) {
    return res.status(404).end('Validation Error');
  }
  dataService.moveNodes(fromArr, toArr);
  io.sockets.except(socketId).emit(needUpdate)
  return res.json(dataService.data);
});

app.post(dataEndpoint + '/paste', (req, res) => {
  const { fromArr, to, isCut, asChild, socketId } = req.body;

  if (!(indexIsValid(fromArr) && indexIsValid(to) && indexIsValid(fromArr[0]) && isCut !== undefined && asChild !== undefined)) {
    return res.status(404).end('Validation Error');
  }
  dataService.pasteNodes(fromArr, to, isCut, asChild);
  io.sockets.except(socketId).emit(needUpdate)
  return res.json(dataService.data);
});

app.use(express.static('dist/syncfusion-angular-app'));

app.listen(process.env.PORT || 3000);
